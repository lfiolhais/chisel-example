# A Small ALU in Chisel

This is the ALU that was presented at the Chisel workshop at INESC-ID Lisbon.

This repo has two examples:
* *Basic*: a basic implementation of the ALU without using any of the advanced
  concepts found in Chisel
* *Advanced*: is the same ALU described in basic but with parameterizable
  components
  
## How to use this repo?
To generate the verilog for the basic or the advanced ALU run `make` in the
project root. To run the test bench execute `make test-basic` to use the FIRRTL
interpreter, or `make test-verilator` to use the verilator backend.

Currently, the makefile defaults into building the advanced example. If you want
to change to the basic example, declare a `MODULE` environment variable or
comment/uncomment the `MODULE` variable in the makefile.

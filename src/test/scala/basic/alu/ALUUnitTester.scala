package example.basic.alu

import chisel3.iotesters
import chisel3.iotesters.{ChiselFlatSpec, Driver, PeekPokeTester}

import math.BigInt

class ALUUnitTester(c: ALU) extends PeekPokeTester(c) {
  val n_tests = rnd.nextInt(1000) * 10

  for (i <- 0 until n_tests) {
    // Poke inputs
    val input_A = rnd.nextLong & 0x7fffffffffffffffL
    val input_B = rnd.nextLong & 0x7fffffffffffffffL
    val sel_A   = rnd.nextInt(2) == 0
    val sel_op  = rnd.nextInt(2)

    poke(c.io.input_A, input_A)
    poke(c.io.input_B, input_B)
    poke(c.io.sel_A, sel_A)
    poke(c.io.sel_op, sel_op)

    // Advance simulation two cycles
    step(2)

    // Perform the same computation in scala so that we
    // can compare results with the simulation
    val result = if (sel_A && sel_op == 0) {
      BigInt(input_A) + BigInt(input_B)
    } else if (!sel_A && sel_op == 0) {
      BigInt(input_B) + BigInt(input_A)
    } else if (sel_A && sel_op == 1) {
      BigInt(input_A) - BigInt(input_B)
    } else {
      BigInt(input_B) - BigInt(input_A)
    }

    val valid = result % 2 == 0

    // Peek outputs
    expect(c.io.valid, valid)
    if (valid) {
      expect(c.io.result, result & BigInt("ffffffffffffffff", 16))
    }
  }
}

/**
* This is a trivial example of how to run this Specification
* From within sbt use:
* {{{
* testOnly chutils.test.RegisterFileTester
* }}}
* From a terminal shell use:
* {{{
* sbt 'testOnly chutils.test.RegisterFileTester'
* }}}
*/
class ALUTester extends ChiselFlatSpec {
  private val backendNames = if(firrtl.FileUtils.isCommandAvailable("verilator")) {
    Array("firrtl", "verilator")
  }
  else {
    Array("firrtl")
  }
  for ( backendName <- backendNames ) {
    "ALU" should s"store random data (with $backendName)" in {
      Driver(() => new ALU(), backendName) {
        c => new ALUUnitTester(c)
      } should be (true)
    }
  }
}

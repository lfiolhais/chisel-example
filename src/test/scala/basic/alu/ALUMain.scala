package example.basic.alu

import chisel3._

object ALUMain extends App {
  iotesters.Driver.execute(args, () => new ALU) {
    c => new ALUUnitTester(c)
  }
}

object ALURepl extends App {
  iotesters.Driver.executeFirrtlRepl(args, () => new ALU)
}

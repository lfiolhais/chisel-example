package example.sm

import chisel3._

object SignMagnitudeMain extends App {
  iotesters.Driver.execute(args, () => new Mac(new SignMagnitude(Some(4)), new SignMagnitude(Some(5)))) {
    c => new SignMagnitudeUnitTester(c)
  }
}

object SignMagnitudeRepl extends App {
  iotesters.Driver.executeFirrtlRepl(args, () => new Mac(new SignMagnitude(Some(4)), new SignMagnitude(Some(5))))
}

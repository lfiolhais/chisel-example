package example.sm

import chisel3.iotesters
import chisel3.iotesters.{ChiselFlatSpec, Driver, PeekPokeTester}

class SignMagnitudeUnitTester(c: Mac[SignMagnitude]) extends PeekPokeTester(c) {
  // 3 * 3 + 2 = 11
  poke(c.io.a.sign, 0)
  poke(c.io.a.magnitude, 3)
  poke(c.io.b.sign, 0)
  poke(c.io.b.magnitude, 3)
  poke(c.io.c.sign, 0)
  poke(c.io.c.magnitude, 2)
  expect(c.io.out.sign, 0)
  expect(c.io.out.magnitude, 11)
  // 3 * 3 - 2 = 7
  poke(c.io.c.sign, 1)
  expect(c.io.out.sign, 0)
  expect(c.io.out.magnitude, 7)
  // 3 * (-3) - 2 = -11
  poke(c.io.b.sign, 1)
  expect(c.io.out.sign, 1)
  expect(c.io.out.magnitude, 11)
}

class SignMagnitudeTester extends ChiselFlatSpec {
  implicit object SignMagnitudeRingImpl extends SignMagnitudeRing
  private val backendNames = if(firrtl.FileUtils.isCommandAvailable("verilator")) {
    Array("firrtl", "verilator", "treadle")
  }
  else {
    Array("firrtl")
  }
  for ( backendName <- backendNames ) {
    "SignMagnitude" should s"store random data (with $backendName)" in {
      Driver(() => new Mac(new SignMagnitude(Some(4)), new SignMagnitude(Some(5))), backendName) {
        c => new SignMagnitudeUnitTester(c)
      } should be (true)
    }
  }
}

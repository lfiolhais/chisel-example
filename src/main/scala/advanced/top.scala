package example.advanced.top

import chisel3._

import example.advanced.mem.Memory
import example.advanced.alu.ALU
import example.advanced.alu.AluIO

class Top(val input_w: Int) extends Module {
  val io = IO(new Bundle {
                val in = new AluIO(input_w)
                val data = Output(UInt(input_w.W))
              })

  // Create Modules
  val alu = Module(new ALU(input_w))
  val mem = Module(new Memory(UInt(input_w.W)))

  // Connect
  alu.io.in <> io.in
  mem.io.in <> alu.io.mem

  io.data := mem.io.data_out
}

// This is needed to generate the verilog just for this module. When generating
// the verilog this object will only be needed in the top module.
object Top extends App {
  // Change width here when generating verilog
  val width = 32

  chisel3.Driver.execute(args, () => new Top(width))
}

package example.basic.alu

import chisel3._
import chisel3.util.Cat

class Adder64() extends Module {
  val io = IO(new Bundle{
                // Inputs
                val operands = Input(Vec(2, UInt(64.W)))
                val cin = Input(UInt(1.W))

                // Outputs
                val result = Output(UInt(64.W))
                val cout = Output(Bool())
              })

  // Invert operand_b when subtracting
  val operand_b = Wire(UInt(64.W))
  when (io.cin === 1.U) {
    operand_b := ~io.operands(1)
  } .otherwise {
    operand_b := io.operands(1)
  }

  // First 32-bit adder
  val adder1 = Module(new Adder)
  adder1.io.cin       := io.cin
  adder1.io.operand_a := io.operands(0)(31, 0)
  adder1.io.operand_b := operand_b(31, 0)

  // Second 32-bit adder
  val adder2 = Module(new Adder)
  adder2.io.cin       := adder1.io.cout.asUInt
  adder2.io.operand_a := io.operands(0)(63, 32)
  adder2.io.operand_b := operand_b(63, 32)

  // Final result
  io.result := Cat(adder2.io.result, adder1.io.result)
  io.cout   := adder2.io.cout
}

object Adder64 extends App {
  chisel3.Driver.execute(args, () => new Adder64())
}

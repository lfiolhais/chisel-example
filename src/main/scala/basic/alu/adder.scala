package example.basic.alu

import chisel3._
import chisel3.util.Cat

class Adder() extends Module {
  val io = IO(new Bundle{
                // Inputs
                val operand_a = Input(UInt(32.W))
                val operand_b = Input(UInt(32.W))
                val cin = Input(UInt(1.W))

                // Outputs
                val result = Output(UInt(32.W))
                val cout = Output(Bool())
              })

  val add_result = io.operand_a +& io.operand_b + io.cin

  io.result := add_result(31, 0)
  io.cout   := add_result(32)
}

object Adder extends App {
  chisel3.Driver.execute(args, () => new Adder())
}

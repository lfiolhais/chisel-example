package example.basic.alu

import chisel3._

class ALU() extends Module {
  val io = IO(new Bundle {
                // Inputs
                val sel_A   = Input(Bool())
                val input_A = Input(UInt(64.W))
                val input_B = Input(UInt(64.W))
                val sel_op  = Input(UInt(1.W))

                // Outputs
                val result = Output(UInt(64.W))
                val valid  = Output(Bool())
              })

  // MUXs
  val operand_A = Mux(io.sel_A, io.input_A, io.input_B)
  val operand_B = Mux(io.sel_A, io.input_B, io.input_A)

  // Registers
  val r0 = RegNext(operand_A)
  val r1 = RegNext(operand_B)
  val r2 = RegNext(io.sel_op)

  // Adder
  val adder = Module(new Adder64)
  adder.io.operands(0) := r0
  adder.io.operands(1) := r1
  adder.io.cin         := r2

  // Output
  val r3 = RegInit(0.U(64.W))

  when (!adder.io.result(0)) {
    r3 := adder.io.result
  }

  io.result := r3
  io.valid  := !adder.io.result(0)
}

// This is needed to generate the verilog just for this module. When generating
// the verilog this object will only be needed in the top module.
object ALU extends App {
  chisel3.Driver.execute(args, () => new ALU())
}

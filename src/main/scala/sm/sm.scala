package example.sm

import chisel3._
import dsptools.numbers._

class SignMagnitude(val magnitudeWidth: Option[Int] = None) extends Bundle {
    val sign = Bool()
    val magnitude = magnitudeWidth match {
        case Some(w) => UInt(w.W)
        case None    => UInt()
    }
    def +(that: SignMagnitude): SignMagnitude = {
      val result = Wire(new SignMagnitude())
      val signsTheSame = this.sign === that.sign
      when (signsTheSame) {
        result.sign      := this.sign
        result.magnitude := this.magnitude + that.magnitude
      } .otherwise {
        when (this.magnitude > that.magnitude) {
          result.sign      := this.sign
          result.magnitude := this.magnitude - that.magnitude
        } .otherwise {
          result.sign      := that.sign
          result.magnitude := that.magnitude - this.magnitude
        }
      }
      result
    }
    def -(that: SignMagnitude): SignMagnitude = {
        this.+(-that)
    }
    def unary_-(): SignMagnitude = {
        val result = Wire(new SignMagnitude())
        result.sign := !this.sign
        result.magnitude := this.magnitude
        result
    }
    def *(that: SignMagnitude): SignMagnitude = {
      val result = Wire(new SignMagnitude())
      result.sign := this.sign ^ that.sign
      result.magnitude := this.magnitude * that.magnitude
      result
    }
    override def cloneType: this.type = {
      new SignMagnitude(magnitudeWidth).asInstanceOf[this.type]
    }
}

trait SignMagnitudeRing extends Ring[SignMagnitude] {
    def plus(f: SignMagnitude, g: SignMagnitude): SignMagnitude = {
        f + g
    }
    def times(f: SignMagnitude, g: SignMagnitude): SignMagnitude = {
        f * g
    }
    def one: SignMagnitude = {
        val one = Wire(new SignMagnitude(Some(1)))
        one.sign := false.B
        one.magnitude := 1.U
        one
    }
    def zero: SignMagnitude = {
        val zero = Wire(new SignMagnitude(Some(0)))
        zero.sign := false.B
        zero.magnitude := 0.U
        zero
    }
    def negate(f: SignMagnitude): SignMagnitude = {
        -f
    }

    def minusContext(f: SignMagnitude, g: SignMagnitude): SignMagnitude = ???
    def negateContext(f: SignMagnitude): SignMagnitude = ???
    def plusContext(f: SignMagnitude,g: SignMagnitude): SignMagnitude = ???
    def timesContext(f: SignMagnitude,g: SignMagnitude): SignMagnitude = ???
}

class Mac[T <: Data : Ring](genIn : T, genOut: T) extends Module {
    val io = IO(new Bundle {
        val a = Input(genIn.cloneType)
        val b = Input(genIn.cloneType)
        val c = Input(genIn.cloneType)
        val out = Output(genOut.cloneType)
    })
    io.out := io.a * io.b + io.c
}

// This is needed to generate the verilog just for this module. When generating
// the verilog this object will only be needed in the top module.
object SignMagnitude extends App {
  implicit object SignMagnitudeRingImpl extends SignMagnitudeRing
  chisel3.Driver.execute(args, () => new Mac(
    new SignMagnitude(Some(4)),
    new SignMagnitude(Some(5)))
  )
}

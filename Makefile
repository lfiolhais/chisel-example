PACKAGE?=example
# MODULE?=basic.alu.ALU
# MODULE?=basic.alu.Adder
# MODULE?=basic.alu.Adder64
# MODULE?=advanced.alu.ALU
MODULE?=sm.SignMagnitude
# MODULE?=advanced.top.Top
# MODULE?=fsm.VendingMachine
export SBT_OPTS=-Xss4M -Xmx2G

verilog:
	sbt 'runMain $(PACKAGE).$(MODULE) --target-dir verilog --top-name $(PACKAGE).$(MODULE)'

test-verilator:
	sbt 'test:runMain $(PACKAGE).$(MODULE)Main --backend-name verilator'

test-basic:
	sbt 'test:runMain $(PACKAGE).$(MODULE)Main --backend-name treadle'

repl:
	sbt 'test:runMain $(PACKAGE).$(MODULE)Repl'

.PHONY: clean clean-verilog verilog

clean:
	rm -rf verilog
	rm -rf test_run_dir

clean-verilog:
	rm -rf verilog/$(PACKAGE).$(MODULE)*
